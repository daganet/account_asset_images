# -*- coding: utf-8 -*-
#######################################
#
# Asset Images
# Copyright (C) 2012 DAGANET WEB SOLUTIONS
# All rights reserved.
#
# This software is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.
#
# Authors: Dairon Medina <dairon@daganet.net>
#
#######################################
from osv import osv,fields
from tools.translate import _

class account_asset_asset(osv.osv):
    _inherit = "account.asset.asset"

    _columns = {
        'image_ids':fields.one2many('asset.images', 'asset_id', 'Asset Images'),
    }

account_asset_asset()
