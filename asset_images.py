# -*- coding: utf-8 -*-
#######################################
#
# Asset Images
# Copyright (C) 2012 DAGANET WEB SOLUTIONS
# All rights reserved.
#
# This software is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.
#
# Authors: Dairon Medina <dairon@daganet.net>
#
#######################################
from osv import osv, fields
from tools.translate import _

class asset_images(osv.osv):
    "Assets Image gallery"
    _name = "asset.images"
    _description = __doc__
    _table = "asset_images"

    _columns = {
        'name':fields.char('Image Title', size=100, required=True, help="Specify a name, Ex: Frontal View, Map, etc."),
        'image_db':fields.binary('Image', filters='*.png,*.jpg,*.gif'),
        'asset_id':fields.many2one('account.asset.asset', 'Asset'),
    }
asset_images()
