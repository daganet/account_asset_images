# -*- coding: utf-8 -*-
#######################################
#
# Asset Images
# Copyright (C) 2012 DAGANET WEB SOLUTIONS
# All rights reserved.
#
# This software is licensed as described in the file LICENSE, which
# you should have received as part of this distribution.
#
# Authors: Dairon Medina <dairon@daganet.net>
#
#######################################

{
    "name" : "Asset Images",
    "version" : "0.1",
    "licence": "GPL3",
    "author" : "DaGaNET Web & Open Source Solutions",
    "website" : "http://www.daganet.net",
    "category" : "Added functionality/Asset Extension",
    "depends" : ['base','account_asset'],
    "description": """
    Add images against every asset.
    """,
    "init_xml": [],
    "update_xml": [
        #'security/ir.model.access.csv',
        'views/asset_images_view.xml',
    ],
    "auto_install": False,
    "installable": True,
    "application": False,

}

